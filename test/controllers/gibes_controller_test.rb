require 'test_helper'

class GibesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gibe = gibes(:one)
  end

  test "should get index" do
    get gibes_url
    assert_response :success
  end

  test "should get new" do
    get new_gibe_url
    assert_response :success
  end

  test "should create gibe" do
    assert_difference('Gibe.count') do
      post gibes_url, params: { gibe: { address: @gibe.address, name: @gibe.name } }
    end

    assert_redirected_to gibe_url(Gibe.last)
  end

  test "should show gibe" do
    get gibe_url(@gibe)
    assert_response :success
  end

  test "should get edit" do
    get edit_gibe_url(@gibe)
    assert_response :success
  end

  test "should update gibe" do
    patch gibe_url(@gibe), params: { gibe: { address: @gibe.address, name: @gibe.name } }
    assert_redirected_to gibe_url(@gibe)
  end

  test "should destroy gibe" do
    assert_difference('Gibe.count', -1) do
      delete gibe_url(@gibe)
    end

    assert_redirected_to gibes_url
  end
end
