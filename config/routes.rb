Rails.application.routes.draw do
	get 					'login', 						to: 'sessions#new'
	post 				'login',						to: 'sessions#create'
	delete 				'logout', 					to: 'sessions#destroy'
	root                                       				'static_pages#index'
	get 					'contact',                          to: 'static_pages#contact'
	get					'fields',                           to: 'fields#index'
	get 					'doctors',                          to: 'doctors#index'
	get 					'users',                            to: 'users#index'

	get 					'doctor_login',				to: 'sessions#new_doctor'
	post 				'doctor_login',				to: 'sessions#create_doctor'
	delete				'doctor_logout', 				to: 'sessions#destroy_doctor'

	get					'appointment',					to: 'appointments#new'
	resources :gibes, :fields, :doctors, :users, :appointments
end
