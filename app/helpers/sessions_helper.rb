module SessionsHelper
	def log_in(user)
		session[:user_id] = user.id
	end

	def current_user
		#@current_user = @current_user || User.find_by(id: session[:user_id])
		if (user_id = session[:user_id])
			@current_user = @current_user || User.find_by(id: user_id)
		elsif (user_id = cookies.encrypted[:user_id])
			user	 = User.find_by(id: user_id)
			if user && user.authenticated?(cookies[:remember_token])
				log_in user
				@current_user = user
			end
		end
	end

	def logged_in?
		!current_user.nil?
	end

	def log_out
		forget(current_user)
		session.delete(:user_id)
		@current_user = nil
	end

	def remember(user)
		user.remember
		cookies.permanent.encrypted[:user_id] = user.id
		cookies.permanent[:remember_token] = user.remember_token
	end

	def forget(user)
		user.forget
		cookies.delete(:user_id)
		cookies.delete(:remember_token)
	end

	#Doctors
	def login_in_doctor(doctor)
		session[:doctor_id] = doctor.id

	end

	def current_doctor
		if (doctor_id = session[:doctor_id])
			@current_doctor = @current_doctor || Doctor.find_by(id: doctor_id)
		elsif (doctor_id = cookies.encrypted[:doctor_id])
			doctor = Doctor.find_by(id: doctor_id)
			if doctor && doctor.authenticated?(cookies[:remember_token])
				login_in_doctor doctor
				@current_doctor = doctor
			end
		end
	end

	def logged_in_doctor?
		!current_doctor.nil?
	end

	def log_out_doctor
		forget(current_doctor)
		session.delete(:doctor_id)
		@current_doctor = nil
	end

	def remember_doctor(doctor)
		doctor.remember
		cookies.permanent.encrypted[:doctor_id] = doctor.id
		cookies.permanent[:remember_token] = doctor.remember_token
	end

	def forget_doctor(doctor)
		doctor.forget
		cookies.delete(:doctor_id)
		cookies.delete(:remember_token)
	end

	def correct_doctor?
		current_doctor.eql?(Doctor.find(params[:id]))
	end

	def correct_user?
		current_user.eql?(User.find(params[:id]))
	end


end
