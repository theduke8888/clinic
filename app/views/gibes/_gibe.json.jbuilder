json.extract! gibe, :id, :name, :address, :created_at, :updated_at
json.url gibe_url(gibe, format: :json)
