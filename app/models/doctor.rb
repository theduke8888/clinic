class Doctor < ApplicationRecord
	belongs_to :field
	has_many :appointments, dependent: :destroy
	validates :field_id, presence: true
	attr_accessor :remember_token
	before_save {self.email = email.downcase}
	validates :name, presence: true, length: {maximum: 50}
	validates :license, presence: true, length: {maximum: 30}
	validates :contact, presence: true,length: {maximum: 15}
	validates :address, presence: true, length: {maximum: 250}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	
	validates :email, presence:  true, length: {maximum: 255},
	          format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
	
	has_secure_password
	validates :password_digest, presence: true, length: {minimum: 6}

	# Returns the hash digest of the given string.
	def Doctor.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
					BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end

	def Doctor.new_token
		SecureRandom.urlsafe_base64
	end

	def remember
		self.remember_token = Doctor.new_token
		update_attribute(:remember_digest, Doctor.digest(remember_token))
	end

	def authenticated?(remember_token)
		return false if remember_digest.nil?
		BCrypt::Password.new(remember_digest).is_password?(remember_token)
	end

	def forget
		update_attribute(:remember_digest , nil)
	end

end
