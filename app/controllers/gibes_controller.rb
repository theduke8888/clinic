class GibesController < ApplicationController
  before_action :set_gibe, only: [:show, :edit, :update, :destroy]

  # GET /gibes
  # GET /gibes.json
  def index
    @gibes = Gibe.all
  end

  # GET /gibes/1
  # GET /gibes/1.json
  def show
  end

  # GET /gibes/new
  def new
    @gibe = Gibe.new
  end

  # GET /gibes/1/edit
  def edit
  end

  # POST /gibes
  # POST /gibes.json
  def create
    @gibe = Gibe.new(gibe_params)

    respond_to do |format|
      if @gibe.save
        format.html { redirect_to @gibe, notice: 'Gibe was successfully created.' }
        format.json { render :show, status: :created, location: @gibe }
      else
        format.html { render :new }
        format.json { render json: @gibe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gibes/1
  # PATCH/PUT /gibes/1.json
  def update
    respond_to do |format|
      if @gibe.update(gibe_params)
        format.html { redirect_to @gibe, notice: 'Gibe was successfully updated.' }
        format.json { render :show, status: :ok, location: @gibe }
      else
        format.html { render :edit }
        format.json { render json: @gibe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gibes/1
  # DELETE /gibes/1.json
  def destroy
    @gibe.destroy
    respond_to do |format|
      format.html { redirect_to gibes_url, notice: 'Gibe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gibe
      @gibe = Gibe.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def gibe_params
      params.require(:gibe).permit(:name, :address)
    end
end
