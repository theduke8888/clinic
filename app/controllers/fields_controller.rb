class FieldsController < ApplicationController
	before_action :check_logged_in
	before_action :correct_user, only: [:edit, :update]
	before_action :check_doctor_status


	def index
		@fields = Field.all
	end

	def new
		@field = Field.new
	end

	def show
		@field = Field.find(params[:id])
		#@doctors  = @field.doctors.all
	end

	def create
		@field = Field.new(params_field)
		if @field.save
			flash[:success] = 'Field added'
			redirect_to fields_url
		else
			flash.now[:danger] = 'Error in saving'
			render 'new'
		end
	end

	def edit
		@field = Field.find(params[:id])
	end

	def update
		@field = Field.find(params[:id])
		if @field.update_attributes(params_field)
			flash[:success] = 'Field Updated'
			redirect_to fields_url
		else
			flash.now[:danger] = 'Error in saving'
			render 'edit'
		end
	end

	def destroy
		Field.find(params[:id]).destroy
		#@field.delete
		flash[:success] = 'Field deleted'
		redirect_to fields_url
	end

	private

		def params_field
			params.require(:field).permit(:area)

		end

		def check_doctor_status
			if logged_in_doctor?
				redirect_to root_url
			end
		end


end
