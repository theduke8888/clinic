User.create!(
	   name:					"Gibe S. Tirol",
	   email:					"gibectu@gmail.com",
	   contact: 				Faker::PhoneNumber.cell_phone,
	   address:    			Faker::Address.full_address,
	   password:				"123456",
	   password_confirmation:	"123456",
	   admin: 				true
)

Field.create!(area: "Anesthesiologist")
Field.create!(area: "Cardiothoracic anesthesiology")
Field.create!(area: "Deep inspiration breath-hold")
Field.create!(area: "Emergency medicine")
Field.create!(area: "General surgery")
Field.create!(area: "Internal medicine")
Field.create!(area: "Medical genetics")
Field.create!(area: "Neurosociology")
Field.create!(area: "Neurosurgery")
Field.create!(area: "Obstetrics and gynaecology")
Field.create!(area: "Ophthalmology")
Field.create!(area: "Orthopedic surgery")
Field.create!(area: "Pediatrics")
Field.create!(area: "Preventive healthcare")
Field.create!(area: "Urology")

25.times do |n|
	name                	= Faker::Name.name
	license             	= Faker::Medical::NPI.npi
	email				= "doctor-#{n+1}@myclinic.com"
     contact             	= Faker::PhoneNumber.cell_phone
	address             	= Faker::Address.full_address
	field_id            	= rand(1..15)
	password				= "123456"
	password_confirmation    = "123456"
	Doctor.create!(
			    name:                	name,
			    license:             	license,
			    email:               	email,
			    contact:             	contact,
			    address:             	address,
			    field_id:            	field_id,
			    password: 				password,
			    password_confirmation:	password_confirmation
	)
end

30.times do |n|
	name                	= Faker::Name.name
	email               	= "user-#{n+1}@myclinic.com"
	contact            	 	= Faker::PhoneNumber.cell_phone
	address             	= Faker::Address.full_address
	password				= "123456"
	password_confirmation    = "123456"
	User.create!(
			  name:                  	name,
			  email:                 	email,
			  contact:               	contact,
			  address:               	address,
			  password: 				password,
			  password_confirmation:      password_confirmation
	)
end

