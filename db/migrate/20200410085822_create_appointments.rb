class CreateAppointments < ActiveRecord::Migration[6.0]
  def change
    create_table :appointments do |t|
      t.string :name
      t.string :address
      t.string :contact
      t.date :dob
      t.string :job
      t.date :doa
      t.string :gender
      t.references :doctor, null: false, foreign_key: true

      t.timestamps
    end
  end
end
