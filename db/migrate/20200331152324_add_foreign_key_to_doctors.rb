class AddForeignKeyToDoctors < ActiveRecord::Migration[6.0]
  def change
    add_column :doctors, :field_id, :integer
    add_foreign_key :doctors, :fields
  end
end
